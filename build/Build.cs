using System.Collections.Generic;
using System.IO;
using System.Text;
using Nuke.Common;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.DotCover;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;

//using static Nuke.Common.Tools.DotNet.DotNetTasks;

[CheckBuildProjectConfigurations]
[UnsetVisualStudioEnvironmentVariables]
class Build : NukeBuild
{
    /// Support plugins are available for:
    ///   - JetBrains ReSharper        https://nuke.build/resharper
    ///   - JetBrains Rider            https://nuke.build/rider
    ///   - Microsoft VisualStudio     https://nuke.build/visualstudio
    ///   - Microsoft VSCode           https://nuke.build/vscode
    public static int Main() => Execute<Build>(x => x.Compile);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [Parameter("Whether or not to publish test results")] bool PublishTests = true;
    [Parameter("The buildnumber for the SemVer")] string BuildNumber = "0";

    
    [Solution] readonly Solution Solution;
    [GitRepository] readonly GitRepository GitRepository;
    [GitVersion] readonly GitVersion GitVersion;

    AbsolutePath SourceDirectory => RootDirectory / "src";
    AbsolutePath TestsDirectory => RootDirectory / "tests";
    AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";
    AbsolutePath TestResultsDirectory => RootDirectory / "results";

    Target Clean => _ => _
        .Before(Restore)
        .Executes(() =>
        {
            Logger.Info("Cleaning solution");
            SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            TestsDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            EnsureCleanDirectory(ArtifactsDirectory);
        });

    Target Restore => _ => _
        .Executes(() =>
        {
            DotNetTasks.DotNetRestore(s => s
                .SetProjectFile(Solution));
        });

    Target Compile => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            string version = GitVersion.SemVer;
            if (BuildNumber != "0")
                version += $".{BuildNumber}";
            
            DotNetTasks.DotNetBuild(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .AddProperty("Version", version)
                .EnableNoRestore());
        });

    Target Test => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var projectFiles = TestsDirectory.GlobFiles(@"**\*.csproj");
            foreach (var projectFile in projectFiles)
            {
                DotNetTasks.DotNetTest(o => o
                    .SetProjectFile(projectFile)
                    .When(PublishTests, oo => oo
                        .SetLogger("trx")
                        .SetResultsDirectory(TestResultsDirectory)));
            }
        });

    Target Coverage => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var projectFiles = TestsDirectory.GlobFiles(@"**\*.csproj");

            var dotnetPath = ToolPathResolver.GetPathExecutable("dotnet");

            List<string> snapshots = new List<string>();

            foreach (var projectFile in projectFiles)
            {
                FileInfo pFile = new FileInfo(projectFile);

                string snapshot = TestResultsDirectory / $@"{pFile.Name}.snapshot";

                DotCoverTasks.DotCoverCover(c => c
                    .SetTargetExecutable(dotnetPath)
                    .SetTargetWorkingDirectory(pFile.Directory.FullName)
                    .SetTargetArguments($"test")
                    .SetFilters(GetCoverageFilters())
                    .SetOutputFile(snapshot));
                
                snapshots.Add(snapshot);
            }

            AbsolutePath mainSnapshot = TestResultsDirectory / "coverage.snapshot";

            DotCoverTasks.DotCoverMerge(c => c
                .SetSource(snapshots)
                .SetOutputFile(mainSnapshot));

            DotCoverTasks.DotCoverReport(c => c
                .SetSource(mainSnapshot)
                .SetOutputFile(TestResultsDirectory / "coverage.html")
                .SetReportType(DotCoverReportType.Html));
        });

    private string GetCoverageFilters()
    {
        var sourceProjects = SourceDirectory.GlobFiles(@"**\*.csproj");
        StringBuilder builder = new StringBuilder();
        foreach (AbsolutePath sourceProject in sourceProjects)
        {
            FileInfo file = new FileInfo(sourceProject);
            string name = file.Name.Substring(0, file.Name.Length - file.Extension.Length);
            builder.AppendLine($"+:{name}");
        }

        return builder.ToString();
    }
}

