﻿using System;
using MyCoreProject;
using Xunit;

namespace XUnitTestProject
{
    public class MyTests
    {
        [Fact]
        public void Test1()
        {
            //Arrange
            MyTestableClass myClass = new MyTestableClass();
            
            //Act
            string result = myClass.TestMethod2();
            
            //Assert
            Assert.Equal(string.Empty, result);
        }
    }
}