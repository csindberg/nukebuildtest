﻿using System;
using MyCoreProject;
using NUnit.Framework;

namespace NUnitTestProject
{
    [TestFixture]
    public class MyNTests
    {
        [SetUp]
        public void SetUp()
        {
            
        }

        [Test]
        public void Test1()
        {
            MyTestableClass myClass = new MyTestableClass();
            Assert.AreEqual(string.Empty, myClass.TestMethod1());
        }
    }
}